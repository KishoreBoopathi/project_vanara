// Fill out your copyright notice in the Description page of Project Settings.


#include "LocationMarker.h"
#include "Components/SphereComponent.h"
#include "MainCharacter.h"
#include "QuestLog.h"
#include "Quest.h"

// Sets default values
ALocationMarker::ALocationMarker()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionVolume = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionVolume"));
	RootComponent = CollisionVolume;

}

// Called when the game starts or when spawned
void ALocationMarker::BeginPlay()
{
	Super::BeginPlay();
	
	CollisionVolume->OnComponentBeginOverlap.AddDynamic(this, &ALocationMarker::CollisionVolumeOnOverlapBegin);
	CollisionVolume->OnComponentEndOverlap.AddDynamic(this, &ALocationMarker::CollisionVolumeOnOverlapEnd);

	CollisionVolume->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionVolume->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
	CollisionVolume->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	CollisionVolume->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

}

// Called every frame
void ALocationMarker::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALocationMarker::CollisionVolumeOnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		AMainCharacter* MainCharacter = Cast<AMainCharacter>(OtherActor);
		if (MainCharacter) 
		{
			UQuestLog* QuestLog = MainCharacter->QuestLog;
			for (AQuest* Quest : QuestLog->Quests)
			{
				if (!Quest->bIsQuestCompleted)
					Quest->TargetInteracted.Broadcast(Cast<AActor>(this), MainCharacter);

				if (Quest->bIsQuestCompleted)
					QuestLog->SetActiveQuest(nullptr);
			}
			QuestLog->SetCurrentObjectiveDescription();
		}
	}
}

void ALocationMarker::CollisionVolumeOnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}
