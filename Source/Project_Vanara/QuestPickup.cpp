// Fill out your copyright notice in the Description page of Project Settings.


#include "QuestPickup.h"
#include "MainCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Sound/SoundCue.h"
#include "QuestLog.h"
#include "Quest.h"
#include "MainPlayerController.h"

AQuestPickup::AQuestPickup()
{
	bQuestItemPicked = false;
}

void AQuestPickup::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		AMainCharacter* MainCharacter = Cast<AMainCharacter>(OtherActor);
		if (MainCharacter)
		{
			MainCharacter->SetActiveOverlappingItem(this);
			if (MainCharacter->MainPlayerController)
			{
				FVector ActiveOverlappingItemLocation = this->GetActorLocation();
				MainCharacter->MainPlayerController->InteractingLocation = ActiveOverlappingItemLocation;
				MainCharacter->MainPlayerController->DisplayInteractMessage();
			}
		}
	}

}
void AQuestPickup::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor)
	{
		AMainCharacter* MainCharacter = Cast<AMainCharacter>(OtherActor);
		if (MainCharacter)
		{
			MainCharacter->SetActiveOverlappingItem(nullptr);
			if (MainCharacter->MainPlayerController)
			{
				MainCharacter->MainPlayerController->RemoveInteractMessage();
			}
		}
	}
}

void AQuestPickup::Interact_Implementation(AMainCharacter* MainCharacter)
{
	UQuestLog* QuestLog = MainCharacter->QuestLog;
	for (AQuest* Quest : QuestLog->Quests)
	{
		if (!Quest->bIsQuestCompleted)
			Quest->TargetInteracted.Broadcast(Cast<AActor>(this), MainCharacter);

		if (Quest->bIsQuestCompleted)
			QuestLog->SetActiveQuest(nullptr);
	}
	QuestLog->SetCurrentObjectiveDescription();
	Destroy();
}