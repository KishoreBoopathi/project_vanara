// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractionInterface.h"
#include "QuestPickup.generated.h"

/**
 * 
 */
UCLASS()
class PROJECT_VANARA_API AQuestPickup : public AActor, public IInteractionInterface
{
	GENERATED_BODY()
	
public:
	AQuestPickup();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Quest")
	bool bQuestItemPicked;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Sound")
	class USoundCue* OverlapSound;

	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interface")
	void Interact(AMainCharacter* MainCharacter);
	virtual void Interact_Implementation(AMainCharacter* MainCharacter) override;
};
